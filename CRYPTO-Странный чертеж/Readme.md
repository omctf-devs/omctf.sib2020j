# Странный чертеж

>>>
У злоумышленников обнаружены старые чертежи какого-то шифровального устройства.
Похоже, они использовали симулятор этой шифровальной машины для передачи секретного сообщения: XZK_UVIDAZ_ZA_WDJNOPPJD. Помогите криптоаналитикам расшифровать послание!

Ответ нужно обернуть в flag{}
>>>

---
![image](Kryha.png)

Симулятор шифровальной машины Kryha:  
http://kryptografie.de/kryptografie/chiffre/kryha.htm

Ключ находился на картинке

`flag{THE_SECRET_IS_DISCLOSED}`
